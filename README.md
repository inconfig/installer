# Руководство по использованию скрипта Installer

## Зачем этот скрипт
Главное предназначение скрипта это обертка над cobbler, для корректной и удобной инсталяции Linux систем, и интеграции cobbler c Salt Stack

## Установка:
### Интеграция с SaltStack:
Для выполнения сценариев salt stack во время установки ОС, необходимо внести изменения в исходный код cobbler.
**AHTUNG**: *Это не кординальные изменения, а всего лишь удобная форма записи по факту статичных настроек.*

*Расширение модуля cobbler item_profile.py*
Добавляем в install профиль системы переменные
 - salt_server - адресс master сервера
 - salt_api_port - порт по которому доступны salt-api
 - salt_api_username - Имя пользователя с доступом к salt модулю wheel
 - salt_api_password - Пароль пользователя
 - salt_api_auth - Систему внешней авторизации к salt_api 
 
**Расширяем структуру FIELDS в модуле item_profile.py**
 
	 
	 ["salt_server", "SETTINGS:salt_server", '<<inherit>>', "SaltStack master server", True, "", 0, "str"],
 	 ["salt_user", "SETTINGS:salt_user", '<<inherit>>', "SaltStack Api user", True, "", 0, "srt"],
  	 ["salt_password", "SETTINGS:salt_password", '<<inherit>>', "SaltStack Api password", True, "", 0, "str"],
	 ["salt_api_port", "SETTINGS:salt_api_port", '<<inherit>>', "SaltStack Api port", True, "", 0, "str"],
	 ["salt_eauth", "SETTINGS:salt_eauth", '<<inherit>>', "SaltStack External auth", True, "", 0, "str"]

**Ставим методы заглушки в классе Profile**
    
    def set_salt_server(self):
        pass
    def set_salt_user(self):
        pass
    def set_salt_password(self):
        pass
    def set_salt_api_port(self):
        pass
    def set_salt_eauth(self):
        pass

	 
**Добавляем соотвествующие настроки в /etc/cobbler/settings**
	
	cat << EOF >> /etc/cobbler/settings
	salt_server: salt.example.com
	salt_user: kickstart
	salt_password: ***
	salt_api_port: 8000
	salt_eauth: pam
	EOF
	
**Перезапускаем cobblerd**
	
	systemctl restart cobblerd

*Snippet в котором производиться установка и конфигурация Salt Minion называется salt_stack.ch*

### Установка пакета

#### Копирование kickstart и snippets
Необходимые файлы находяться в дирректории пакета 
Kickstart сценарии: contrib_files/cobbler/lib/kickstarts
Snippets: contrib_files/cobbler/lib/snippets/
	
	$ export COBBLER_LIB=/var/lib/cobbler
	$ cp -r /usr/lib/python2.7/site-packages/installer_lib/contrib_files/cobbler/snippets/* ${COBBLER_LIB}/snippets/
	$ cp -r /usr/lib/python2.7/site-packages/installer_lib/contrib_files/cobbler/kickstarts/* ${COBBLER_LIB}/kickstarts/

#### Настройка профилей
Для install профиля каждого дистрибутива необходимо отредактировать параметр kickstart
	
	$ cobbler profile edit --name <profile_name> --kickstart <path_to_installer_kickstart>
	
#### Создание конфигурационного файла для работы скрипта
Пример конфигурационного файла находиться в cp -r /usr/lib/python2.7/site-packages/installer_lib/contrib_files/rc_config_example.yaml

**Пример заполнения конфигурации**
**AHTUNG: Это не готовый пример для копирования**
	
	server: cobbler-install-server.example.com
	username: cobbler
	password: cobbler
	profile_pattern:
	ksmeta_vars:
  	    - rfc
	    - part_scheme
	    - post_script

	networks:
	    3102:
            netmask: 255.255.255.0
            gateway: 10.31.129.3
	    3109:
	        netmask: 255.255.255.0
	        gateway: 10.31.161.3
	        
*Пояснения к опциям:*
- server: IP/Hostname cobbler сервера
- username: Логин для доступа к API
- password: Пароль пользователя к API
- ksmeta_vars: Список параметров коммандной строки, которые добавляются в качестве meta переменных к профилю системы, менять в данной диррективе ничего не нужно
- networks: Данные для по конфигурации сетей install сервера. 3102, 3109 - vlan id в данном случае.
**AHTUNG: Данный файл записан в yaml формате, вместо TAB используйте 4 пробела для отделения уровней**

*Скрипт installer поддерживает указание пути до конфигурационного файла. Поумолчанию конфиг файл должен находиться в домашней дирректории пользователя и называться ".INSTALLER.rc"*

### Работа со скриптом
*Help к сприпту доступен по команде*
	
	$installer --help
	
*Привет КО*

**Список доступных для исталяции профилей**
	
	$installer profile -l
	
Будет выдан список профилей  с которых возможна исталяция, при это вывод указанной команды и соманды $cobbler profile list могут не совпадать есть в конфигурационном файле указана опция profile_pattern

**Список Систем добавленых в cobbler**
	
	$installer system list
	
**Удаление системы из cobbler**
	
	$installer system rm -n <имя системы>
	
**Добавление системы в cobbler**
	
	$installer system add -n <имя системы> -p <имя профиля> -mac <МАС address> -ip <ip аddress> -v_id <vlan id> --rfc <номер RFC>
	
все указанные ключи являются обязательными, не обязательные ключи --part_scheme --post_script, позволяют указать snippet со схемой париций и post install скрипт, указанные файлы дожны находиться ниже каталого /var/lib/cobbler/snippets

**Синхронизация cobbler сервера**
Для выполнения команды cobbler sync после добавления или удаления системы необходимо добавить ключь --sync перед позиционным агрументом system
	
	$installer --sync system add ...
	
### Проверки производимые скриптом

Скрипт осуществляет проверку следующих параметров:
- Система уже добавлена в cobbler(В данном случае параметры исходной системы будут перезаписаны новыми)
- Ключ с именем системы уже присутствует на salt master сервере(в этом случае будет предпринята попытка удаления ключа с salt master сервера)

### Тестируемые возможности(в ходе работы могут возникнуть ошибки, исключения не обрабатываются)

**Массовое добавление систем на основе файла**
	
	$installer system install_file -P <путь к файлу>
	
Будет произведено чтение всех парамеров из указанного файла, файл при этом должен быть заполненин в следующем формате:
	
	<system name>@<install_profile>@<ip_addr>@<mac>@<vlan_id>

*Пример*
	
		salt-minion-tst@current-rhel-server-7.2-x86_64@10.31.129.208@00:1a:4a:16:01:77@3102
		salt-minion-tst-2@current-rhel-server-7.2-x86_64@10.31.129.209@00:1a:4a:16:01:89@3102
		
		
#### TODO:
- Добавить проверку вводимого ip, mac адресса
- Добавить проверку соотвествия введенного ip адрессу сети, добавить опцию net_addr в конфигурационный файл
- Реализовать возможность добавления state сценариев из командной строки
- Заменить(где необходимо) print на logging
- Unit тесты !!!
- Возможность задать dns домен отличный от vimpelcom.ru
- Срипты для сбора информации с ILO, RSC, корзин etc
- Добавить возможность отслеживать статус исталяции
	
	