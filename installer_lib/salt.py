# encoding: utf-8
from __future__ import unicode_literals
from __future__ import absolute_import

#  Import Salt Pepper lib
from pepper.libpepper import Pepper, PepperException

class ExtendPepper(Pepper, object):
    '''
        Extend basic class of Pepper
    '''
    login_info = None

    def wheel(self, fun, arg=None, kwarg=None, **kwargs):
        '''
            Run sigle command of wheel client
        '''
        low = {
            'client': 'wheel',
            'fun': fun,
        }

        if arg:
            low['arg'] = arg
        if kwarg:
            low['kwarg'] = kwarg

        low.update(kwargs)

        return self.low([low], path='/')


    def wheel_keys_all(self):
        return self.wheel('key.list_all').get('return').pop().get('data').get('return')

    def w_key_exists(self, mid):
        if not filter(lambda x: mid in x, self.wheel_keys_all().values()):
            return False
        else:
            return True

    def w_key_delete(self, mid):
        self.wheel('key.delete', match=mid)
        if not self.w_key_exists(mid):
            return True
        else:
            return False

    def __init__(self, user, password, eauth, api_url='https://localhost:8000', debug_http=False, ignore_ssl_errors=False):
        super(ExtendPepper, self).__init__(api_url, debug_http, ignore_ssl_errors)
        self.login_info = self.login(user, password, eauth)