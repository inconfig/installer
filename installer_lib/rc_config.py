# encoding: utf-8
'''
    Config parser module
'''
#Import python libs
from __future__ import unicode_literals
from __future__ import absolute_import
import os
import yaml

from installer_lib.misc import term_colors as tc_print

# Exceptions
class ConfigValidateError(Exception):
    pass

class ConfigReadError(Exception):
    pass

VALID_OPTS_TYPES = {
    'server': str,
    'username': str,
    'password': str,
    'networks': dict,
    'netmask': str,
    'gateway': str,
    'profile_pattern': str,
    'ksmeta_vars': list,
}

def _validate_(opts):
    '''
        Validate options types
    :param opts:
    :return: bool
    '''
    error = ("Invalid value of {0}, type is {1} required type for this option {2}")
    errors = []

    if not isinstance(opts, dict):
        raise ConfigValidateError(
            'Options has invalid type {0} required {1}'.format(
                type(opts).__name__,
                dict.__name__,
            )
        )
    for key, val in opts.items():
        if key in VALID_OPTS_TYPES.keys():
            if isinstance(val, VALID_OPTS_TYPES[key]):
                continue

            if isinstance(val, (list, dict)):
                _validate_(val)
            if hasattr(VALID_OPTS_TYPES[key], '__call__'):
                try:
                    VALID_OPTS_TYPES[key](val)
                except (ValueError,TypeError):
                    errors.append(
                        error.format(
                            key,
                            type(val).__name__,
                            VALID_OPTS_TYPES[key].__name__,
                        )
                    )

    if errors:
        [tc_print().fail(err) for err in errors]
        return False

    return True

def _read_file_(path):
    '''
        Read config from yaml file
    :param path: str
    :return: dict or None
    '''
    config = None
    if not os.path.exists(path):
        raise OSError("No such file {0}".format(path))

    with open(path, 'r') as fd:
        try:
            config = yaml.load(fd)
        except yaml.YAMLError as err:
            tc_print().fail(err)
        finally:
            fd.close()
            return config

def load_config(path):
    '''
        Wrapper function for _read_file and _validate_
    :param path: str
    :return: dict
    '''
    config = _read_file_(path)

    if config is None:
        raise ConfigReadError("Load configuration from {0} FAIL".format(path))

    if _validate_(config):
        return config
    else:
        raise ConfigValidateError("Config validation fail")


