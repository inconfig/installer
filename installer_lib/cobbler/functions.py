# encoding: utf-8
'''
    Completed function working with cobbler rpc server
'''
from __future__ import unicode_literals
from __future__ import absolute_import
import os

from installer_lib.misc import term_colors as tc
from installer_lib.misc import zip_list_to_dict

def systems_from_file(namespace_args, lines_from_file, config, cobbler_server, continue_on_error=False, verbose=True):
    '''
        Read systems parameters from file and add to cobbler
        AHTUNG: File format must be same
        name@install_profile@ipaddr@hwaddr@vlan_id

    :param f_path: str
    :param config: dict
    :param cobbler_server: obj
    :return tuple(bool, errors:tuple)
    '''
    parsed_lines = map(lambda l: l.strip('\n').split('@'), lines_from_file)

    systems_opts = map(
                    lambda x: zip_list_to_dict(
                        ['name', 'profile', 'ip', 'mac', 'netmask', 'gateway'],
                        x
                    ),
                    map(
                        lambda xl,yl: xl + yl,
                        map(
                            lambda l: l[:-1],
                            parsed_lines
                        ),
                        filter(
                            lambda s: s is not None,
                            map(
                                lambda v: [
                                    config['networks'][int(v)]['netmask'],
                                    config['networks'][int(v)]['gateway'],
                                ],
                                map(
                                    lambda l: l[-1],
                                    parsed_lines
                                )
                            )
                        )
                    )
                )

    fails = []
    while systems_opts:
        opts = systems_opts.pop()
        opts.update({'ks_meta': ks_meta(namespace_args, config['ksmeta_vars']), 'update': True})
        try:
            cobbler_server.new_system(**opts)
        except Exception as err:
            tc().fail('ERROR: {0}'.format(err))
            if continue_on_error:
                fails.append(opts['name'])
            else:
                return (
                    False, (opts['name'],)
                )
        else:
            tc().okgreen("System {0} add OK".format(opts['name']))

    if fails:
        return (
            False, tuple(fails)
        )

    return (
        True, ()
    )

def ks_meta(namespace_args, meta_vars_list):
    attrs = []
    for attr_name in meta_vars_list:
        val = getattr(namespace_args, attr_name)
        if val:
            attrs.append(
                "{k}={v}".format(k=attr_name, v=val)
            )
    if attrs:
        return ' '.join(attrs)
    else:
        return None