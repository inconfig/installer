# encoding: utf-8
'''
    Call remote cobbler functions via xmlrpclib
'''
#Import python libs
from __future__ import unicode_literals
from __future__ import absolute_import

import xmlrpclib
from installer_lib.misc import term_colors as tc_print

# Exceptions
class ConnectionError(Exception):
    pass

class LoginError(Exception):
    pass


class RemoteCobbler(object):
    '''
        Remote execution cobbler functions
    '''
    # Define Static attributes and data structs
    COBBLER_URL = "http://{0}/cobbler_api"

    # Network interface attrs
    INTERFACE_OPTIONS_STRUCTURE = {
        'macaddress-eth0': None,
        'ipaddress-eth0': None,
        'dnsname-eth0': None,
        'netmask-eth0': None,
        'static-eth0': True,
    }
    def __init__(self, host='localhost', username='cobbler', password='cobbler'):
        # Connect to host
        code, err = self._connect_(host)
        self._check_code_(code, ConnectionError, err)

        #Login
        code, err = self._login_(username, password)
        self._check_code_(code, LoginError, err)


    def _check_code_(self, code, exception, err):
        if not code:
            raise exception(err)

    def _connect_(self, host):
        '''
            Connect to RPC server
        :param host: str
        :return: tuple(bool, err or None)
        '''
        try:
            self.rpc_server = xmlrpclib.Server(self.COBBLER_URL.format(host))
        except Exception as err:
            return (False, err)
        else:
            return (True, None)

    def _login_(self, username, password):
        '''
            Login to RPC server
        :param username: str
        :param password: str
        :return: tuple(bool, err or None)
        '''
        try:
            self.auth_token = self.rpc_server.login(username, password)
        except Exception as err:
            return (False, err)
        else:
            return (True, None)

    def sync(self):
        return self.rpc_server.sync(self.auth_token)

    def settings(self):
        '''
            Return cobbler settings dict
        :return: dict
        '''
        return self.rpc_server.get_settings()

    def systems(self):
        '''
            Return list of systems
        :return: list
        '''
        return sorted(map(lambda x: x['name'], self.rpc_server.get_systems()))

    def repos(self):
        '''
            Return list of repos
        :return: list
        '''
        return sorted(map(lambda x: x['name'], self.rpc_server.get_repos()))

    def new_system(self, name, profile, ip, mac, netmask, gateway, update=False, dns_domain='example.com',**kwargs):
        '''
            Create new system object
        :param name: str
        :param profile: str
        :param ip: str
        :param mac: str
        :param vlan: str
        :param rfc: str
        :return:
        '''
        def set_parameter(system_id, parameter_name, parameter_val, token):
            tc_print().default("Set system parameter {0} to {1}".format(parameter_name, parameter_val))
            self.rpc_server.modify_system(system_id, parameter_name, parameter_val, token)

        def setup_network(**kwargs):
            for key in self.INTERFACE_OPTIONS_STRUCTURE.keys():
                self.INTERFACE_OPTIONS_STRUCTURE[key] = kwargs[key]

        def save_system(system_id, token):
            try:
                self.rpc_server.save_system(system_id, token)
            except Exception as err:
                return (False, err)
            else:
                return (True, None)

        if update:
            system_id = self.rpc_server.get_system_handle(name, self.auth_token)
        else:
            system_id = self.rpc_server.new_system(self.auth_token)

        #Setup system
        setup_network(
            **{
                'macaddress-eth0': mac,
                'ipaddress-eth0': ip,
                'dnsname-eth0': '{0}.{1}'.format(name, dns_domain),
                'netmask-eth0': netmask,
                'static-eth0': True,
            }
        )
        if kwargs.get('ks_meta') is not None:
            parameters = {
                'name': name,
                'hostname': '{0}.vimpelcom.ru'.format(name),
                'profile': profile,
                'modify_interface': self.INTERFACE_OPTIONS_STRUCTURE,
                'gateway': gateway,
                'ks_meta': kwargs['ks_meta']
            }
        else:
            parameters = {
                'name': name,
                'hostname': '{0}.{1}'.format(name, dns_domain),
                'profile': profile,
                'modify_interface': self.INTERFACE_OPTIONS_STRUCTURE,
                'gateway': gateway,
            }

        for name, val in parameters.items():
            set_parameter(system_id, name, val, self.auth_token)

        r_code_save, err = save_system(system_id, self.auth_token)
        if not r_code_save:
            return (False, name, err)

        return (True, name, None)

    def remove_system(self, name):
        '''
            Function remove passed system
        :param name: str
        :return: bool
        '''
        try:
            self.rpc_server.remove_system(name, self.auth_token)
        except Exception as err:
            return (False, name, err)
        else:
            return (True, name, None)

    def profiles(self, profile_pattern = None):
        '''
            Function for list profiles
        :param profile_pattern: str
        :return: list
        '''
        if profile_pattern is not None:
            profiles = sorted(
                filter(
                    lambda x: x.startswith(profile_pattern),
                    map(lambda x: x['name'], self.rpc_server.get_profiles())
                )
            )
        else:
            profiles = sorted(
                map(lambda x: x['name'], self.rpc_server.get_profiles())
            )
        return profiles
