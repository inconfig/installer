#!/usr/bin/env python
# encoding: utf-8

#Import python libs
from __future__ import unicode_literals
from __future__ import absolute_import
from sys import exit, path
import os
import argparse

path.append('./')
from installer_lib.rc_config import load_config
from installer_lib.misc import term_colors as tc_print
from installer_lib.cobbler import RemoteCobbler
from installer_lib.cobbler.functions import systems_from_file
from installer_lib.cobbler.functions import ks_meta
from installer_lib.salt import ExtendPepper

if os.uname()[0] != 'Linux':
    tc_print().fail('OS not supported exit')
    exit(1)

global _config_

try:
    HOME = os.environ['HOME']
    _config_ = load_config('{0}/.INSTALLER.rc'.format(HOME))
except OSError:
    pass

def exit_with_error(message, code=1):
    tc_print().fail(message)
    exit(code)

def arg_parser():
    parser = argparse.ArgumentParser(description="Script wrapper for cobbler")
    parser.add_argument('-s', '--sync', action='store_true', dest='SYNC', help='Run cobbler sync after operation')
    parser.add_argument('-v', '--verbose', action='store_true', dest='VERB', help='Verbose output')
    parser.add_argument('-C', '--config', action='store', dest='CONFIG', help='Config file path')
    # Create sub_parsers
    sub_parsers = parser.add_subparsers(dest='object')

    #Create System sub_parser
    system_parser = sub_parsers.add_parser('system', help='Functions for working with system objects')
    system_sub_parsers = system_parser.add_subparsers(dest='command')
    system_parser.set_defaults(func=_system_wrapper_)
    ## Create system list sub parser
    system_sub_parsers.add_parser('list', help='List systems')
    ## Create system add sub parser
    system_add_parser = system_sub_parsers.add_parser('add', help='Add system for install')
    system_add_parser.add_argument('-n', '--name', action='store', dest='system_name', help='System name', required=True)
    system_add_parser.add_argument('-ip', '--ipaddr', action='store', dest='system_ip', help='System ipaddr', required=True)
    system_add_parser.add_argument('-mac', '--hwaddr', action='store', dest='system_mac', help='System mac addr', required=True)
    system_add_parser.add_argument('-p', '--profile', action='store', dest='install_profile', help='Install profile', required=True)
    system_add_parser.add_argument('-v_id', '--vlan', action='store', dest='vlan_id', help='Vlan ID', required=True)
    system_add_parser.add_argument('--rfc', action='store', dest='rfc', help='RFC number', required=True)
    system_add_parser.add_argument('--partitioning', action='store', dest='part_scheme', help='Custom partition scheme')
    system_add_parser.add_argument('--post-script', action='store', dest='post_script', help='Post install script')
    system_add_parser.add_argument('--domain', action='store', dest='dns_domain', help='DNS Domain name')
    # Add systems from text file
    system_add_from_file_parser = system_sub_parsers.add_parser('install_file', help='Get options from file')
    system_add_from_file_parser.add_argument('--rfc', action='store', dest='rfc', help='RFC number')
    system_add_from_file_parser.add_argument('--partitioning', action='store', dest='part_scheme', help="Custom partition scheme")
    system_add_from_file_parser.add_argument('--post-script', action='store', dest='post_script', help='Post install script')
    system_add_from_file_parser.add_argument('-P', '--path', action='store', dest='install_file', help='Path to file', required=True)
    ## Create system remove sub parser
    system_rm_parser = system_sub_parsers.add_parser('rm', help='Remove system from installer')
    system_rm_parser.add_argument('-n', '--name', action='store', dest='system_name', help='System name')

    #Create profile sub_parser
    profile_parser = sub_parsers.add_parser('profile', help='Function for working with profiles objects')
    profile_parser.add_argument('-l', '--list', action='store_true', dest='list_profiles', help='List profiles')
    profile_parser.set_defaults(func=_profile_wrapper_)
    # Add config list parser
    config_parser = sub_parsers.add_parser('config', help='List installer configuration')
    config_parser.add_argument('-n', '--networks', '-v_ids', '--vlans', action='store_true', dest='config_networks', help='List networks config options')
    config_parser.set_defaults(func=_config_wrapper_)
    #TODO: add parsers for functions working with SaltStack

    return parser.parse_args()

def _system_wrapper_(args):
    global _config_
    cobbler_server = RemoteCobbler(
        _config_['server'],
        _config_['username'],
        _config_['password'],
    )

    def system_add(args):
        if args.VERB:
            opts_list = [
                'NAME:{0}'.format(args.system_name),
                'PROFILE:{0}'.format(args.install_profile),
                'IP:{0}'.format(args.system_ip),
                'MAC:{0}'.format(args.system_mac),
                'VLAN:{0}'.format(args.vlan_id),
                'RFC:{0}'.format(args.rfc)
            ]
            for opt in opts_list:
                tc_print().header(opt)

        # Check Salt key exists if True remove them
        try:
            salt_api = ExtendPepper(
                cobbler_server.settings()['salt_user'],
                cobbler_server.settings()['salt_password'],
                cobbler_server.settings()['salt_eauth'],
                "https://{0}:{1}".format(
                    cobbler_server.settings()['salt_server'],
                    cobbler_server.settings()['salt_api_port'],
                ),
            )
        except Exception as err:
            exit_with_error('ERROR: {0} when connect to salt-api on {1}'.format(err, cobbler_server.settings()['salt_server']))
        else:
            if salt_api.w_key_exists(args.system_name):
                tc_print().warning(
                    'WARNING: Key {0} already exist on {1}, key will be remove'.format(
                        args.system_name,
                        cobbler_server.settings()['salt_server']
                    )
                )
                if not salt_api.w_key_delete(args.system_name):
                    tc_print().fail(
                        'ERROR: Remove key {0} from {1} fail'.format(
                            args.system_name,
                            cobbler_server.settings()['salt_server'],
                        )
                    )

        # Check is system already exits in cobbler
        if args.system_name in cobbler_server.systems():
            tc_print().warning(
                "WARNING: System with name {0} already exits in installer, parameters will be rewrite".format(args.system_name)
            )
            update = True
        else:
            update = False


        # Check passed profile in existing profile
        if not args.install_profile in cobbler_server.profiles(profile_pattern=_config_['profile_pattern']):
            exit_with_error(
                'ERROR: Profile {0} missing in cobbler profiles. EXIT: 1'.format(args.install_profile),
                1
            )

        # Check passed vlan_id in _config_
        if not int(args.vlan_id) in _config_['networks'].keys():
            exit_with_error(
                'ERROR: Vlan ID {0} missing in configuration. EXIT: 1'.format(args.vlan_id),
                1
            )

        if args.dns_domain is None:
            dns_domain = 'vimpelcom.ru'
        else:
            dns_domain = args.dns_domain

        # Add system to installer
        cobbler_server.new_system(
            name=args.system_name,
            profile=args.install_profile,
            ip=args.system_ip,
            mac=args.system_mac,
            netmask=_config_['networks'][int(args.vlan_id)]['netmask'],
            gateway=_config_['networks'][int(args.vlan_id)]['gateway'],
            update=update,
            dns_domain=dns_domain,
            ks_meta=ks_meta(
                args, _config_['ksmeta_vars'],
            )
        )
        if args.SYNC:
            tc_print().header("Sync cobbler server")
            cobbler_server.sync()

        tc_print().okgreen("System add OK")
        exit(0)

    def system_rm(args):
        # Check system in cobbler systems
        if not args.system_name in cobbler_server.systems():
            exit_with_error(
                'ERROR: System {0} missing in cobbler systems, nothing todo'.format(args.system_name),
                1
            )

        tc_print().header('INFO: Removed system {0}'.format(args.system_name))
        cobbler_server.remove_system(args.system_name)

        if args.SYNC:
            tc_print().header("INFO: Sync cobbler server")
            cobbler_server.sync()

        tc_print().okgreen('INFO: System {0} remove OK'.format(args.system_name))
        exit(0)

    def system_list():
        for system in cobbler_server.systems():
            tc_print().bold(system)
        exit(0)


    tc_print().underline('System {0}:'.format(args.command))

    if args.command == 'list':
        system_list()

    elif args.command == 'add':
        system_add(args)

    elif args.command == 'rm':
        system_rm(args)

    elif args.command == 'install_file':
        with open(args.install_file) as fd:
            rcode, fails = systems_from_file(args, fd.readlines(), _config_, cobbler_server)
            if not rcode:
                map(lambda f: tc_print().fail("System {0} add FAIL".format(f)), fails)
                exit(1)
    else:
        exit_with_error('Command not supported', 1)



def _profile_wrapper_(args):
    cobbler_server = RemoteCobbler(
        _config_['server'],
        _config_['username'],
        _config_['password'],
    )

    def profiles_list():
        tc_print().underline('Profiles:')
        for profile in cobbler_server.profiles(profile_pattern=_config_['profile_pattern']):
            tc_print().bold(profile)
        exit(0)

    if args.list_profiles:
        profiles_list()
    else:
        exit(1)

def _config_wrapper_(args):

    if args.config_networks:
        for vlan, opts in _config_['networks'].items():
            print("{0}: {1}".format(vlan, opts))
        exit(0)

def main():
    args = arg_parser()
    if args.CONFIG is not None:
        global _config_
        _config_ = load_config(args.CONFIG)

    args.func(args)

if __name__ == '__main__':
    main()