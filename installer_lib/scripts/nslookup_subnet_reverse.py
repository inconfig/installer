#!/usr/bin/env python
# encoding: utf-8

import sys, os
import argparse
from subprocess import call
from installer_lib.ns_functions import nslookup_reverse

FD_NULL = open(os.devnull, 'a')
UNAME = os.uname()[0]
PRINT_PATTERN = "{0:<16}{1:<20}{2:<32}{3:^4}\n"

def args_parser():
    parser = argparse.ArgumentParser(description='Script for check subnet A records')
    parser.add_argument('-n', '--net', action='store', dest='network_addr', help='Network address. Example: 192.168.1.0',
                        required=True)
    parser.add_argument('-fl', '--filter', action='store', dest='filter', choices=['free', 'used'],
                        help='Query filter')
    parser.add_argument('-p', '--ping', action='store_true', dest='ping', help='Ping test')
    parser.add_argument('-o', '--out', action='store', dest='output', help='Output destination file. Default stdout')
    parser.set_defaults(func=_main)
    return parser.parse_args()

def _lookup_status(scan_item):
    if scan_item['hostname'] is None:
        scan_item['status'] = 'FREE'
    else:
        scan_item['status'] = 'USED'
    return scan_item

def _ping(addr, ping=False):
    if not ping:
        return 'Ping test disable'

    if UNAME == 'Linux': CMD = 'ping -W 1 -c 1 {0}'
    if UNAME == 'SunOS': CMD = 'ping -W 1 {0}'

    ping = call(CMD.format(addr), shell=True, stdout=FD_NULL, stderr=FD_NULL)

    if ping == 0:
        return 'Avail'
    else:
        return 'Not responding'

def _main(args):
    network = args.network_addr.split('.')[0:-1]
    start_addr = int(args.network_addr.split('.')[-1])
    ips = ["{0}.{1}".format('.'.join(network),i) for i in xrange(start_addr,255)]
    scan_result = map(
        lambda ip: {'ipaddr': ip, 'hostname': nslookup_reverse(ip), 'ping_status': _ping(ip, args.ping)},
        ips
    )
    scan_result = map(_lookup_status, scan_result)

    if args.filter == 'free':
        scan_result = filter(lambda x: x['status'] == 'FREE', scan_result)
    elif args.filter == 'used':
        scan_result = filter(lambda x: x['status'] == 'USED', scan_result)

    if args.output is not None:
        with open(args.output, 'w') as fdout:
            map(
                lambda x: fdout.write(PRINT_PATTERN.format(x['ipaddr'], x['ping_status'], x['hostname'], x['status'])),
                scan_result
            )
    else:
        map(
            lambda x: sys.stdout.write(PRINT_PATTERN.format(x['ipaddr'], x['ping_status'], x['hostname'], x['status'])),
            scan_result
        )

    sys.exit(0)

def main():
    args = args_parser()
    try:
        args.func(args)
    except KeyboardInterrupt:
        print('\nAborted by user')
        exit(1)

if __name__ == '__main__':
    args = args_parser()
    try:
        args.func(args)
    except KeyboardInterrupt:
        print('\nAborted by user')
        exit(1)