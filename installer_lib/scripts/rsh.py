#!/usr/bin/env python
# encoding: utf-8

import sys, os
import argparse
import json
import getpass
import logging
from uuid import uuid4
from time import asctime
from installer_lib.misc import term_colors as tc_print
from installer_lib import ssh_functions

HISTORY_DIR = '{0}/.rhn-history/'.format(os.environ['HOME'])
TASK_FILE = uuid4().hex
tc_print().bold('Task id {0}'.format(TASK_FILE))

if not os.path.exists(HISTORY_DIR):
    os.mkdir(HISTORY_DIR)

def arg_parser():
    parser = argparse.ArgumentParser(description='Execute remote command via ssh')
    parser.add_argument('-o', '--output', action='store', dest='output_file',
                        help='Write output to file in json format')
    sub_parsers = parser.add_subparsers(dest='method')

    one_off_parser = sub_parsers.add_parser('one_off', help='SSH to one server')
    one_off_parser.add_argument('-H', '--host', action='store', dest='host', help='Hostname of ssh server',
                                required=True)
    one_off_parser.add_argument('-U', '--user', action='store', dest='user', help='SSH username',
                                required=True)
    one_off_parser.add_argument('-C', '--command', action='store', dest='command', help='Command to execute')
    one_off_parser.add_argument('-F', '--command-file', action='store', dest='cmd_file',
                                help='Read and execute commands from file')
    one_off_parser.set_defaults(func=one_off_main)

    fabric_parser = sub_parsers.add_parser('fabric', help='SSH to servers fabric')
    fabric_parser.add_argument('-IN', '--input-sequence', action='store', dest='input_sequence',
                               help='Input sequence. Read hostnames from file', required=True)
    fabric_parser.add_argument('-P', '--passwords', action='store', dest='passwords_sequence',
                               help='Read passwords from file')
    fabric_parser.add_argument('-C', '--command', action='store', dest='command', help='Command to execute')
    fabric_parser.add_argument('-cf', '--command-file', action='store', dest='command_file',
                               help='Read and execute commands from file')
    fabric_parser.set_defaults(func=fabric_main)
    return parser.parse_args()

def one_off_main(args):
    hostname = args.host
    username = args.user
    password = getpass.getpass(prompt='password:')

    if args.cmd_file is not None:
        with open(args.cmd_file, 'r') as fd_cmd:
            commands = map(lambda x: x.strip('\n'), fd_cmd.readlines())
    else:
        if args.command is not None:
            commands = [args.command]
        else:
            print('Missing command or command_file argument')
            sys.exit(1)

    client = ssh_functions.get_ssh_client(hostname, username, password)
    sys.stdout.write('Command to execute\n')
    map(
        lambda c: sys.stdout.write('+{0}\n'.format(c)),
        commands
    )

    result = map(
        lambda c: ssh_functions.remote_command(client, c),
        commands
    )

    if args.output_file is not None:
        with open(args.output_file, 'w') as fd_out:
            json.dump({'result': result, 'host': hostname}, fd_out)
    else:
        map(sys.stdout.write, result)


def fabric_main(args):
    stdin = False
    username = 'root'

    with open(args.input_sequence, 'r') as fd_in:
        hosts = map(lambda stdin: stdin.strip('\n'), fd_in.readlines())
    if not hosts:
        tc_print().fail('Missing hosts')
        sys.exit(1)

    if not stdin: username = raw_input('username:')
    if args.passwords_sequence is None:
        if not stdin: passwords = [getpass.getpass(prompt='password:')]
        else: sys.exit(1)
    else:
        with open(args.passwords_sequence, 'r') as fd_passwd_in:
            passwords = filter(
                lambda line: line != '',
                map(lambda passwd: passwd.strip('\n'), fd_passwd_in.readlines())
            )

    if args.command_file is not None:
        with open(args.command_file, 'r') as fd_cmd_in:
            commands = filter(
                lambda line: line != '',
                map(lambda line: line.stip('\n'), fd_cmd_in.readlines())
            )
    elif args.command is not None:
        commands = [args.command]
    else:
        tc_print().fail('Missing command or command-file arg')
        sys.exit(1)

    ssh_clients_raw = map(lambda h: ssh_functions.pick_ssh_client(h, username, passwords), hosts)
    ssh_client_none = filter(lambda client: client[1] is None, ssh_clients_raw)
    ssh_clients = filter(lambda client: client[1] is not None, ssh_clients_raw)
    del ssh_clients_raw

    if ssh_client_none:
        tc_print().warning('Fabric connection fail to clients:')
        map(lambda client: sys.stdout.write('{0}\n'.format(client[0])), ssh_client_none)
    if ssh_clients:
        tc_print().bold('Fabric connection successfully to clients:')
        map(lambda client: sys.stdout.write('{0}\n'.format(client[0])), ssh_clients)

    if not stdin:
        verify = False
        while not verify:
            question = raw_input('Continue? (y/n): ')
            if question not in ['y', 'n']:
                continue
            elif question == 'n':
                tc_print().bold('Exit. Bye')
                sys.exit(1)
            elif question == 'y':
                verify = True


    tc_print().bold('Commands will be executed:')
    map(lambda c: sys.stdout.write('+ {0}\n'.format(c)), commands)

    if not stdin:
        verify = False
        while not verify:
            question = raw_input('Continue? (y/n): ')
            if question not in ['y', 'n']:
                continue
            elif question == 'n':
                tc_print().bold('Exit. Bye')
                sys.exit(1)
            elif question == 'y':
                verify = True

    result = {}
    for client in ssh_clients:
        host, ssh_client = client
        result[host] = map(lambda rcmd: ssh_functions.remote_command(ssh_client, rcmd), commands)
        map(lambda r: sys.stdout.write('\n{0}\n{1}\n'.format(host, r)), result[host])
        ssh_client.close()

    with open('{0}/{1}'.format(HISTORY_DIR, TASK_FILE), 'w') as fd_out:
        result['time'] = asctime()
        json.dump(result, fd_out)

    if args.output_file is not None:
        with open(args.output_file, 'w') as fd_out:
            json.dump(result, fd_out)

def main():
    args = arg_parser()
    args.func(args)


if __name__ == '__main__':
    args = arg_parser()
    args.func(args)

