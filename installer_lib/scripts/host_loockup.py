#!/usr/bin/env python
# encoding: utf-8

import sys, os
import argparse
from installer_lib.ns_functions import nslookup

PRINT_PATTERN = '{0:<18}{1:<32}\n'

def args_parser():
    parser = argparse.ArgumentParser(description='Script for nslookup using list sequence')
    parser.add_argument('-in', '--input', action='store', dest='input', help='Input sequence. Default STDIN')
    parser.add_argument('-o', '--out', action='store', dest='output', help='Output. Default STDOUT')
    parser.add_argument('-fl', '--filter', action='store', dest='filter', choices=['exists', 'not_exists'],
                        help='Result filter')
    parser.set_defaults(func=_main)
    return parser.parse_args()

def _main(args):
    if args.input is None:
        hosts_names = map(lambda x: x.strip('\n'), sys.stdin.readlines())
    else:
        with open(args.input, 'r') as fdin:
            hosts_names = fdin.readlines()


    scan = map(
        lambda h: {'hostname': h, 'ipaddr': nslookup(h)},
        hosts_names
    )

    if args.filter == 'exists':
        scan = filter(lambda s: s['ipaddr'] is not None, scan)
    elif args.filter == 'not_exists':
        scan = filter(lambda s: s['ipaddr'] is None, scan)

    if args.output is not None:
        with open(args.output, 'w') as fdout:
            map(
                lambda h: fdout.write(PRINT_PATTERN.format(h['ipaddr'], h['hostname'])),
                scan
            )
    else:
        map(
            lambda h: sys.stdout.write(PRINT_PATTERN.format(h['ipaddr'], h['hostname'])),
            scan
        )

def main():
    args = args_parser()
    try:
        args.func(args)
    except KeyboardInterrupt:
        sys.exit(1)

if __name__ == '__main__':
    args = args_parser()
    try:
        args.func(args)
    except KeyboardInterrupt:
        sys.exit(1)