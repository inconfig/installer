# encoding: utf-8

import socket

def nslookup(hostname):
    '''
        Lookup host ip addr in DNS using hostname
    :param hostname: str
    :return: str or None
    '''
    try:
        addr = socket.gethostbyname(hostname)
    except socket.gaierror:
        addr = None
    #print addr

    return addr

def nslookup_reverse(addr):
    '''
        Lookup hostname in DNS using address
    :param addr: str
    :return: str or None
    '''
    try:
        hostname = socket.gethostbyaddr(addr)[0]
    except socket.gaierror:
        hostname = None
    except socket.herror:
        hostname = None

    return hostname

