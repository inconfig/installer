'''
    Misc functions
'''
from operator import add

class term_colors(object):
    '''
        ASCII Codes for terminal colors
    '''
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    def _clprint_(self, color, message):
        print(
            '{color}{msg}{endc}'.format(
                color=color,
                msg=message,
                endc=self.ENDC
            )
        )

    def header(self, message):
        self._clprint_(self.HEADER, message)

    def okblue(self, message):
        self._clprint_(self.OKBLUE, message)

    def okgreen(self, message):
        self._clprint_(self.OKGREEN, message)

    def warning(self, message):
        self._clprint_(self.WARNING, message)

    def fail(self, message):
        self._clprint_(self.FAIL, message)

    def bold(self, message):
        self._clprint_(self.BOLD, message)

    def underline(self, message):
        self._clprint_(self.UNDERLINE, message)

    def default(self, message):
        print(message)

def merge_lists(a,b):
    '''
        Return merged tuple
    :param a: tuple or list
    :param b: tuple or list
    :return: tuple
    '''
    return add(a,b)

def zip_list_to_dict(a, b):
    return dict(zip(a,b))