#!/usr/bin/env bash

COBBLER_LIB_PATH='/var/lib/cobbler'

#Checking environment
if [ $(uname) != 'Linux' ]; then
    printf 'This package must be run on Linux system'
    exit 1
fi

if [ ! -d $COBBLER_LIB_PATH ]; then
    printf '/var/lib/cobbler not available\n';
    exit 1
fi

if [ $DIRSTACK != '/' ]; then
    printf 'Start this script from / path';
    exit 1
fi

DIR=$(echo "$0"|sed 's/bootstrap.sh//g')

#Copy kickstarts -> COBBLER_LIP_PATH/kickstarts
cp ${DIR}/lib/kickstarts/*.ks ${COBBLER_LIB_PATH}/kickstarts/
#Copy snippets
cp -r ${DIR}/lib/snippets/* ${COBBLER_LIB_PATH}/snippets/
#TODO: implement setup profiles procedure