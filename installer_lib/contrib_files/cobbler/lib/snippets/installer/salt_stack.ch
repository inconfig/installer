# Setup Salt Stack Minion

# Create directories, redifine paths vars if you need
#set $pki_dir = '/etc/salt/pki/minion'
#set $config_dir = '/etc/salt/minion.d'
mkdir -p $pki_dir
mkdir -p $config_dir

# Get minion pregenerated keys
#set $mid = $getVar('name', None)
curl -sSk https://$salt_server:$salt_api_port/keys \
    -d mid=$mid \
    -d username=$salt_user \
    -d password=$salt_password \
    -d eauth=$salt_eauth \
    -o /tmp/salt-keys.tar \
# Unpack keys
tar xf /tmp/salt-keys.tar -C $pki_dir

# Create minion configuration
printf 'master: $salt_server' >> $config_dir/master.conf
printf 'id: $mid' >> $config_dir/id.conf

# Start service
service salt-minion start

# Wait 5 seconds
sleep 5

# Call highstate
salt-call state.highstate
