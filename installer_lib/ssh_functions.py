# encoding: utf-8

import paramiko
import sys

def get_ssh_client(host, user, password, port=22):
    '''
        Simple Connect to ssh host using paramiko
    :param host: str
    :param user: str
    :param password: str
    :param port: int
    :return: paramiko.SSHClient or None
    '''
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(
            hostname=host,
            username=user,
            password=password,
            port=port,
        )
    except paramiko.AuthenticationException:
        client = None

    return client

def pick_ssh_client(host, user, passwords, port=22):
    '''
        Get paramiko.ssh client by selecting password from passwords arg
    :param host: str
    :param user: str
    :param passwords: list
    :param port: int
    :return: paramiko.SSHClient or None
    '''
    client = None
    i = 0
    for passwd in passwords:
        sys.stderr.write('try {0:<16} on {1:<32} [{2}/{3}]\r'.format(passwd,host,i,len(passwords)-1))
        i += 1
        client = get_ssh_client(host, user, passwd, port)
        if client is not None:
            sys.stderr.write('\nPassword for {0} is {1}\n'.format(host, passwd))
            break

    return (host, client)

def remote_command(client, command):
    '''
        Execute remote ssh command
    :param client: paramiko.SSHClient
    :param command: str
    :return: str
    '''
    stdin, stdout, stderr = client.exec_command(command)
    return (stdout.read() + stderr.read())