from setuptools import setup, find_packages
from os.path import join, dirname
from os import uname

UNAME = uname()[0]
__version__ = '1.2.1'

if UNAME == 'Linux':
    console_scripts = [
        'installer = installer_lib.scripts.installer: main',
        'pnslookup_subnet_reverse = installer_lib.scripts.nslookup_subnet_reverse: main',
        'host_loockup = installer_lib.scripts.host_loockup: main',
        'rsh = installer_lib.scripts.rsh: main',
    ]
elif UNAME == 'SunOS':
    console_scripts = [
        'pnslookup_subnet_reverse = installer_lib.scripts.nslookup_subnet_reverse: main',
        'host_loockup = installer_lib.scripts.host_loockup: main',
        'rsh = installer_lib.scripts.rsh: main',
    ]
else:
    console_scripts = []

setup(
    name='installer',
    version=__version__,
    description='Wrapper script for cobbler xmlrpc API',
    license='MIT',
    author='Denis V Sonin',
    author_email='dv.sonin@jet.msk.su',
    packages=find_packages(),
    include_package_data=True,
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    url='http://gitlab.vimpelcom.ru/jetinf/installer',
    install_requires=[
        'PyYAML>=3.12',
    ],
    entry_points={
        'console_scripts': console_scripts
    },
)

