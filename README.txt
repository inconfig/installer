#Руководство по использованию скрипта Installer

##Зачем этот скрипт
Главное предназначение скрипта это обертка над cobbler, для корректной и удобной инсталяции Linux систем, и интеграции cobbler c Salt Stack

##Установка:
###Интеграция с SaltStack:
Для выполнения сценариев salt stack во время установки ОС, необходимо внести изменения в исходный код cobbler.
**AHTUNG**: *Это не кординальные изменения, а всего лишь удобная форма записи по факту статичных настроек.*

*Расширение модуля cobbler item_profile.py*
Добавляем в install профиль системы переменные
 - salt_server - адресс master сервера
 - salt_api_port - порт по которому доступны salt-api
 - salt_api_username - Имя пользователя с доступом к salt модулю wheel
 - salt_api_password - Пароль пользователя
 - salt_api_auth - Систему внешней авторизации к salt_api 
 
** Расширяем структуру FIELDS в модуле item_profile.py**
 
	 
	 ["salt_server", "SETTINGS:salt_server", '<<inherit>>', "SaltStack master server", True, "", 0, "str"],
 	 ["salt_user", "SETTINGS:salt_user", '<<inherit>>', "SaltStack Api user", True, "", 0, "srt"],
  	 ["salt_password", "SETTINGS:salt_password", '<<inherit>>', "SaltStack Api password", True, "", 0, "str"],
	 ["salt_api_port", "SETTINGS:salt_api_port", '<<inherit>>', "SaltStack Api port", True, "", 0, "str"],
	 ["salt_eauth", "SETTINGS:salt_eauth", '<<inherit>>', "SaltStack External auth", True, "", 0, "str"]
	 
**Добавляем соотвествующие настроки в /etc/cobbler/settings**
	
	cat << EOF >> /etc/cobbler/settings
	salt_server: salt.example.com
	salt_user: kickstart
	salt_password: ***
	salt_api_port: 8000
	salt_eauth: pam
	EOF
	
**Перезапускаем cobblerd**
	
	systemctl restart cobblerd

*Snippet в котором производиться установка и конфигурация Salt Minion называется salt_stack.ch*

###Установка пакета


